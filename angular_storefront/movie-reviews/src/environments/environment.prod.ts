export const environment = {
  production: true,
  moviesServiceUrl: 'http://movies.info/api/movies',
  movieReviewServiceUrl: 'http://reviews.info/api/reviews',
  movieRatingServiceUrl: 'http://ratings.info/api/ratings'
};
