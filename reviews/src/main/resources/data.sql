INSERT INTO MOVIE_REVIEW (movie_id, review, rating) VALUES (1, 'Brilliant performance from Tom Hardy!', 8.2);
INSERT INTO MOVIE_REVIEW (movie_id, review, rating) VALUES (2, 'Brad Pitt in his first major role...!', 9.1);
INSERT INTO MOVIE_REVIEW (movie_id, review, rating) VALUES (2, 'I didn t understand the plot...!', 5);
INSERT INTO MOVIE_REVIEW (movie_id, review, rating) VALUES (3, 'Another hit from the one and only tarantino', 8.1);
INSERT INTO MOVIE_REVIEW (movie_id, review, rating) VALUES (4, 'A classic!', 9.2);
